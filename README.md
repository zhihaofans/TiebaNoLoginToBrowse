欢迎提出意见
#TiebaNoLoginBrowse
在未登录贴吧时点下一页时会自动识别弹出的登录界面地址，并跳转到下一页。

理论上所有地址前面的是 "http://tieba.baidu.com/f/user/passport?jumpUrl=" 都可以用。

在页面里弹出的登录窗口暂时不行。

**更新内容**

2015.8.28:
修复bug:中文名吧会跳转到 "http://tieba.baidu.comhttp//tieba.baidu.com/" ,英文名字吧可以用。
